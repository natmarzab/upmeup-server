import { HttpException, HttpStatus, Injectable, Logger, Scope } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { randomBytes } from "crypto";
import { User } from "src/users/models/user";
import { setTimeout } from "timers/promises";

const CREATE_USER_URL = "/api/v1/users.create";
const LOGIN_URL = "/api/v1/login";
const INSTANT_MESSAGES_URL = "/api/v1/im.list";
const USER_INFO_URL = "/api/v1/users.info";
const UPDATE_USER_URL = "/api/v1/users.update";
const CREATE_IM_URL = "/api/v1/im.create";

@Injectable({ scope: Scope.DEFAULT })
export class RocketChatService {
    private host: string;
    private username: string;
    private password: string;

    constructor(private configService: ConfigService) {
        this.host = configService.get('ROCKETCHAT_ENDPOINT');
        this.username = configService.get('ROCKETCHAT_ADMIN_USERNAME');
        this.password = configService.get('ROCKETCHAT_ADMIN_PASSWORD');
    }

    async createUser(user: User) {
        const adminCredentials = await this.adminLogin()
        let username = this.generateUsername(user);
        if (await this.usernameExists(username)) {
            username += `.${randomBytes(2).toString('hex')}`;
        }
        const result = await fetch(`${this.host}${CREATE_USER_URL}`, {
            method: 'POST',
            body: JSON.stringify({
                email: user.email,
                name: user.type === '1' ? `${user.name} ${user.surname}` : user.name,
                password: user.chatPassword,
                username
            }),
            headers: {
                "Content-type": "application/json",
                "X-Auth-Token": adminCredentials.authToken,
                "X-User-Id": adminCredentials.userId
            }
        });
        Logger.debug('Rocket.Chat response on user creation');
        Logger.debug(result);
        if (result.ok){
            return this.login(user.email, user.chatPassword);
        }
        Logger.error(`Failed to create rocketChat User for user: ${user.email}, ${username} : ${user.name} ${user.surname}`);
        throw new HttpException('INTERNAL SERVER ERROR', HttpStatus.INTERNAL_SERVER_ERROR);
    }

    async updateUser(user: User): Promise<boolean> {
        const userInfo = await this.getUserInfo(user.chatUserId);
        if (!userInfo || !userInfo.username)
            return false;
        const { userId, authToken } = await this.adminLogin();
        if (userId && authToken) {
            let username = this.generateUsername(user);
            const existingUser = await this.usernameExists(username);
            if (existingUser && existingUser != user.chatUserId) {
                username += `.${randomBytes(2).toString('hex')}`;
            }
            const result = await fetch(`${this.host}${UPDATE_USER_URL}`, {
                method: 'POST',
                body: JSON.stringify({
                    userId: user.chatUserId,
                    data: {
                        username,
                        name: user.type === '1' ? `${user.name} ${user.surname}` : user.name,
                        email: user.email
                    }
                }),
                headers: {
                    "Content-type": "application/json",
                    "X-Auth-Token": authToken,
                    "X-User-Id": userId
                }
            });
            return result.ok;
        }
        return false;
    }

    async getUserTokens(userId: string, password: string) {
        const userInfo = await this.getUserInfo(userId);
        if (userInfo) {
            return this.login(userInfo.username, password);
        }
    }

    async createConversation(chatUserId: string, password: string, chatRecipientId: string): Promise<string> {
        const userInfo = await this.getUserTokens(chatUserId, password);
        const recipientInfo = await this.getUserInfo(chatRecipientId)
        const result = await fetch(`${this.host}${CREATE_IM_URL}`, {
            method: 'POST',
            body: JSON.stringify({username: recipientInfo.username}),
            headers: {
                "Content-type": "application/json",
                "X-Auth-Token": userInfo.authToken,
                "X-User-Id": userInfo.userId
            }
        });
        if(result.ok) {
            return (await result.json()).room.rid;
        }
        return;
    }

    async getChats(userId: string, authToken: string) {
        const result = await fetch(`${this.host}${INSTANT_MESSAGES_URL}`, {
            headers: {
                "X-Auth-Token": authToken,
                "X-User-Id": userId
            }
        })
        if (!result.ok)
            return [];
        const chats = await result.json();
        return chats.ims;
    }

    private async login(email: string, password: string): Promise<any> {
        const result = await fetch(`${this.host}${LOGIN_URL}`, {
            method: 'POST',
            body: JSON.stringify({ user: email, password }),
            headers: {
                "Content-type": "application/json"
            }
        });
        if (result.ok) {
            const { data: { userId, authToken } } = await result.json();
            return { userId, authToken };
        }
        return undefined;
    }


    private async usernameExists(username: string): Promise<string> {
        const adminCredentials = await this.adminLogin();

        const result = await fetch(`${this.host}${USER_INFO_URL}?username=${username}`, {
            headers: {
                "Content-type": "application/json",
                "X-Auth-Token": adminCredentials.authToken,
                "X-User-Id": adminCredentials.userId
            }
        })
        return result.ok ? (await result.json()).user._id : undefined;
    }

    private async adminLogin() {
        const adminCredentials = this.login(this.username, this.password);
        if (!adminCredentials) {
            Logger.error('Failed admin login for rocket.chat integration. Please check configured credentials are correct');
            throw new HttpException('INTERNAL SERVER ERROR', HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return adminCredentials;
    }

    private async getUserInfo(userId: string, authUserId: string = undefined, authToken: string = undefined) {
        const adminCredentials = (authToken && authUserId) ? { authToken, userId: authUserId } : await this.adminLogin();
        const result = await fetch(`${this.host}${USER_INFO_URL}?userId=${userId}`, {
            headers: {
                "Content-type": "application/json",
                "X-Auth-Token": adminCredentials.authToken,
                "X-User-Id": adminCredentials.userId
            }
        })
        if (result.ok) {
            return (await result.json()).user
        }
    }

    private generateUsername(user: User) {
        return `${user.name.toLowerCase().trim().normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/\.+/g, '').replace(/ +/g, '.')}${user.type === '1' ? '.' : ''}${user.type == '1' ? user.surname.toLowerCase().trim().normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/\.+/g, '').replace(/ +/g, '.') : ''}`;
    }
}