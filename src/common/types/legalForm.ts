export enum LegalForm {
    association="association",
    cooperative="cooperative",
    insertioncompany="insertioncompany",
    foundation="foundation",
    laboursociety="laboursociety",
    mutuals="mutuals",
    specialemploymentcenters="specialemploymentcenters",
    othereesentity="othereesentity"
}

