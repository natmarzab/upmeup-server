/* eslint-disable prettier/prettier */
import { Field, InputType } from '@nestjs/graphql';
import { Schema } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { CandidatureStatus } from 'src/common/types/candidatureStatus';
import { OfferStatus } from 'src/common/types/offerStatus';
import { RequiredExperience } from 'src/common/types/requiredExpirience';
import { Competencies } from 'src/competencies/models/competence';

export type CompanyOfferDocument = UpdateOfferInput & mongoose.Document;

@InputType({ description: 'Update Offer document' })
export class UpdateOfferInput {
    @Field({ nullable: true })
    title: string;
  
    @Field({ nullable: true })
    eduLevel: string;

    @Field({ nullable: true })
    city: string;
  
    @Field({ nullable: true })
    salaryRange: string;
  
    @Field({ nullable: true })
    remote: string;
  
    @Field({ nullable: true })
    contractType: string;
  
    @Field({ nullable: true })
    workHours: string;

    @Field(() => [Competencies], { nullable: true })
    competencies: Competencies[];

    @Field({ nullable: true })
    description: string;

    @Field({ nullable: true })
    requirements: string;

    @Field(() => Number, { nullable: true })
    enrolled: number;

    @Field(() => [CandidateInput], { nullable: 'itemsAndList'})
    candidates: CandidateInput[]

    @Field(() => RequiredExperience, { nullable: true })
    requiredExperience: RequiredExperience;  

    @Field(() => OfferStatus , { nullable: true })
    status: OfferStatus;

    @Field(() => [String], { nullable: true })
    file: string[];
}

@InputType()
export class CandidateInput {

    @Field(() => String) 
    user: String;

    @Field(() => CandidatureStatus , { nullable: true })
    status: CandidatureStatus;
}