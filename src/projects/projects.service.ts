import { BadRequestException, Inject, Injectable, Logger } from "@nestjs/common";
import { CONTEXT } from "@nestjs/graphql";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { OfferStatus } from "src/common/types/offerStatus";
import { User } from "src/users/models/user";
import { UsersService } from "src/users/users.service";
import { CreateProjectDto } from "./dto/create-project.dto";
import { ProjectFilter } from "./inputs/project-filter.input";
import { UpdateProjectInput } from "./inputs/update-project.input";
import { Project, ProjectDocument } from "./models/projects";


@Injectable()
export class ProjectsService {
    constructor(@Inject(CONTEXT) private context,
                @InjectModel(Project.name) private projectModel: Model<ProjectDocument>,
                private uService: UsersService){}
    
    async getProject(_id: string): Promise<Project> {
      // const loggedUser = await this.uService.getUserById(this.context.user._id);
      // const matchFilter = { $text: { $search: `${loggedUser.lastJobTasks} ${loggedUser.jobPosition}` }, _id: _id }
      // const matchOffers = (await this.coffModel.find(matchFilter, { score: { $meta: "textScore" } }).populate([{ path: 'user' }, { path: 'candidates.user', model: User.name }])) as any;
      let project;
      // if(matchOffers.length == 0){
        project = await this.projectModel.findById(_id).populate([{ path: 'user' }, { path: 'user', model: User.name }]) as any;
        // offer.match = this.calculateMatch(offer, loggedUser).total;
        return project;
      // }
      // offer = matchOffers[0];
      // offer.match = this.calculateMatch(offer, loggedUser).total;
      // return offer;
    }

    async getAllProjects(userID?: string,filter?: ProjectFilter, limit?: number, skip?: number) {
      const formatedFilter: any = filter

      if (filter) {
        if (filter.createdDate)
          formatedFilter.createdDate = { $gt: filter.createdDate }
        if (filter.search) {
          formatedFilter.$text = { $search: filter.search }
          delete formatedFilter.search
        }
        if (filter.sector) {
          formatedFilter["sector._id"] = filter.sector
          delete formatedFilter.sector
        }

      }

      try {
        let matchFilter;
        let projects;

        //Since we need to use the text index both to filter and to calculate the match, if there is a filter present we need to retrieve the offers in two steps
        if (formatedFilter) {
          
          projects = (await this.projectModel
            .find(formatedFilter)
            .skip(skip)
            .limit(limit)
            .populate([{ path: 'user' }, { path: 'user', model: User.name }])
            .sort({ createdDate: -1 })) as any;

        } else { 
          
          if(userID){
            projects = await this.projectModel
          .find({'userId': userID })
          .populate([{ path: 'user' }, { path: 'user', model: User.name }])
          }
          else {
            projects = await this.projectModel
          .find()
          .populate([{ path: 'user' }, { path: 'user', model: User.name }])
          }
          
        }

        return projects;

      } catch (error) {
        Logger.error(error);
        throw new BadRequestException();
      }
    }

    async getInterestProjects(userId: string) {
      return this.projectModel.find({ "interestedUsers._id": userId }).populate([{ path: 'user' }, {
        path: 'interestedUsers._id',
        model: User.name
      }])
    }
    
    async createProject(createProjectDto: CreateProjectDto): Promise<Project> {
        const createdItem = new this.projectModel(createProjectDto);
        const createdProject = await createdItem.save();
        const maxScoreProject = await this.projectModel.findOne({ $or: [{ $text: { $search: `${createdProject.title} ${createdProject.description}` }, 
                                                                                  _id: createdProject._id }, 
                                                                        { _id: createdProject._id }] }, 
                                                                { score: { $meta: "textScore" } }) as any;
      
        await this.projectModel.findByIdAndUpdate(createdProject._id, { maxMatchScore: maxScoreProject._doc.score });
        return createdProject;
    }

    async getProjectByIdAndOwnerId(_id: string, ownerId): Promise<Project> {
      const project = (await this.projectModel.findOne({ _id })).admins;
      if (JSON.stringify(project).includes(ownerId) === true){
        return this.projectModel.findOne({ _id });
      }
    }

    async updateProject(_id: string, updateProjectInput: UpdateProjectInput) {
      const updatedProject = await this.projectModel.findByIdAndUpdate(_id, updateProjectInput, { new: true }).populate([{ path: 'user' }, { path: 'user', model: User.name }]);
      const maxScoreProject = await this.projectModel.findOne({ $or: [{ $text: { $search: `${updatedProject.title} ${updatedProject.description}` },
                                                                                 _id: updatedProject._id },
                                                                      { _id: updatedProject._id }] }, 
                                                              { score: { $meta: "textScore" } }) as any;
      await this.projectModel.findByIdAndUpdate(updatedProject._id, { maxMatchScore: maxScoreProject._doc.score });
      return updatedProject;
    }

    /**
     *  - Retrieve user (name) by Id
     *  - Retrieve project 
     *  - Check if user is already in the array
     *    - (if not) Save interested user (Id, name) into array field `interestedUsers`
     *      of model 'Project'
     *      
     */
    async addInterestedUser(projectId: string, interestedUserId: string) {

      Logger.log(`addInterestedUser :: project : ${projectId} : interestedUserId : ${interestedUserId}`)
      
      // Fetch user - only name should be required
      const interestedUser = await this.uService.getUserById(interestedUserId);
      Logger.debug(`interestedUser: ${JSON.stringify(interestedUser,null,2)}`) // debug
  
      // Fetch project
      let project = await this.projectModel.findOne({ _id: projectId }).populate('user') as any;
      Logger.debug(`project: ${JSON.stringify(project,null,2)}`) // debug
      
      const interestedUsers = project.interestedUsers as Array<any>;
      Logger.debug (`interestedUsers: ${JSON.stringify(interestedUsers,null,2)}`) // debug
      
      const interestedUserIndex = interestedUsers.findIndex(u => u._id === interestedUserId);
      Logger.debug (`interestedUserIndex: ${interestedUserIndex}`) // debug
      
      const user_present = (interestedUserIndex !== -1)
      
      // update project.interestedUsers[] array
      if (!user_present) {
        
        Logger.log (`user_present: ${user_present} : adding ${interestedUserId}`) // debug
        // Add to array
        const projectInput = new UpdateProjectInput();
        interestedUsers.push({ _id: interestedUserId, name: interestedUser.name});
        projectInput.interestedUsers = interestedUsers;
        Logger.debug (`Adding user :: projectInput: ${JSON.stringify(projectInput,null,2)}`) // debug
        // Update
        project = await this.projectModel.findByIdAndUpdate(projectId, projectInput, { new: true }).populate([{ path: 'user' }, { path: 'interestedUsers.user', model: User.name }]);
        Logger.debug(`Updated project: ${JSON.stringify(project,null,2)}`) // debug
      }
      else {
        Logger.log (`addInterestedUser :: user ${interestedUserId} ALREADY present in project : ${projectId}`)
      }
      
      return project
    
    }

    /**
     *  - Fetch project by projectId
     *  - Check if user is already in the array (by _id)
     *  - Remove interested user from array `interestedUsers`
     *      of model 'Project'
     */
    async removeInterestedUser(projectId: string, interestedUserId: string) {

      Logger.log(`removeInterestedUser :: projectId : ${projectId} : interestedUserId : ${interestedUserId}`)
      
      // Fetch project
      let project = await this.projectModel.findOne({ _id: projectId }).populate('user') as any;
      Logger.debug(`project: ${JSON.stringify(project,null,2)}`) // debug
      
      const interestedUsers = project.interestedUsers as Array<any>;
      Logger.debug (`interestedUsers: ${JSON.stringify(interestedUsers,null,2)}`) // debug
      
      const interestedUserIndex = interestedUsers.findIndex(u => u._id === interestedUserId);
      Logger.debug (`interestedUserIndex: ${interestedUserIndex}`) // debug
      
      const user_present = (interestedUserIndex !== -1)
      
      // update project.interestedUsers[] array
      if (user_present) {
        // remove if present
        Logger.log (`user_present: ${user_present} : removing ${interestedUserId}`) // debug
        const projectInput = new UpdateProjectInput();
        interestedUsers[interestedUserIndex] = { _id: interestedUserId }
        interestedUsers.splice(interestedUserIndex,1) // Remove from array
        projectInput.interestedUsers = interestedUsers;
        Logger.debug (`Removing user :: projectInput: ${JSON.stringify(projectInput,null,2)}`) // debug
        // Update
        project = await this.projectModel.findByIdAndUpdate(projectId, projectInput, { new: true }).populate([{ path: 'user' }, { path: 'interestedUsers.user', model: User.name }]);
        Logger.debug(`Updated project: ${JSON.stringify(project,null,2)}`) // debug
      }
      else {
        Logger.log (`removeInterestedUser :: user ${interestedUserId} NOT present in project : ${projectId}`)
      }
    
      return project;
      
    }
    
    async updateProjectState(pId: string, state: OfferStatus): Promise<Project> {
      await this.projectModel.updateOne(
        { "_id": pId},
        { "$set": { "state": state } }
      );
      return this.projectModel.findOne({ "_id": pId});
    }

    async deleteProject(_id: string): Promise<Project> {
      const deletedProject = await this.projectModel.findByIdAndDelete(_id)
  
      return deletedProject;
    }

    async filterByContent (loggedUser, projectID?){
      let matchFilter;
      let projects;

      projects = await this.projectModel
        .find()
        .populate([{ path: 'user' }, { path: 'user', model: User.name }])


      matchFilter = { $text: { $search: `${loggedUser.lastJobTasks} ${loggedUser.jobPosition}` }, _id: { $in: projects.map(p => p._id) }, active: true };
      
      

      if (projectID){
        matchFilter = { $text: { $search: `${loggedUser.lastJobTasks} ${loggedUser.jobPosition}` }, _id: projectID, active: true };
      }

      const matchProjects = await this.projectModel.find(matchFilter, { score: { $meta: "textScore" } }) as any;
      
      return matchProjects;

    }
}