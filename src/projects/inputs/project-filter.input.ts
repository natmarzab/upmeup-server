import { Field, InputType } from "@nestjs/graphql";

@InputType()
export class ProjectFilter {
    
    @Field(() => Date, { nullable: true })
    createdDate: Date

    @Field(() => String, { nullable: true })
    city: string;

    @Field(() => String, { nullable: true })
    status: string;
  
    @Field(() => String, { nullable: true })
    sector: string;

    @Field(() => String, { nullable: true })
    search: string;


}