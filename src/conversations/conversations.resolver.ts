import { Context, Resolver, Query } from "@nestjs/graphql";
import { RocketChatService } from "src/common/rocketchat/rocketchat.service";
import { UsersService } from "src/users/users.service";
import { Conversation } from "./models/convesation";
import { UseGuards } from "@nestjs/common";
import { AuthGuard } from "src/auth/auth.guard";

@Resolver(() => Conversation)
export class ConversationResolver {

  constructor(private readonly usersService: UsersService, private readonly chatService: RocketChatService) { };

  @UseGuards(AuthGuard)
  @Query((returns) => [Conversation], { name: 'conversations' })
  async getConversations(@Context() context) {
    const conversations: Conversation[] = [];
    const loggedUser = await this.usersService.getUserById(context.user._id);
    const rocketChatUser = await this.chatService.getUserTokens(loggedUser.chatUserId, loggedUser.chatPassword);
    const chats = await this.chatService.getChats(rocketChatUser.userId, rocketChatUser.authToken);
    for (const chat of chats) {
      if (chat.msgs > 0) {
        const recipientId = chat.uids.find(uid => uid !== loggedUser.chatUserId);
        const recipient = await this.usersService.getUserByChatUserId(recipientId);
        const lastMessage = chat.lastMessage ? { text: chat.lastMessage.msg, timeSent: chat.lastMessage.ts } : undefined;
        conversations.push({
          _id: chat._id,
          messageCount: chat.msgs,
          recipient: recipient,
          lastMessage: lastMessage
        })
      }
    }
    return conversations;
  }
}