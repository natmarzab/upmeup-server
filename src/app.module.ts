import { CacheModule, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './common/modules/database/database.module';
import { GraphQLModule as GraphQL } from './common/modules/graphql/graphql.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { CompanyOffersModule } from './company-offers/company-offers.module';
import { SoftskillsModule } from './softskills/softskills.module';
import { SectorsModule } from './sectors/sectors.module';
import { CompetenciesModule } from './competencies/competencies.module';
import { ConfigModule } from '@nestjs/config';
import { ProjectModule } from './projects/projects.module';
import { ConversationsModule } from './conversations/conversations.module';
import { MatchModule } from './match/match.module';
import { ConversationsService } from './conversations/conversations.service';

@Module({
  imports: [
    ProjectModule,
    MatchModule,
    DatabaseModule,
    GraphQL,
    UsersModule,
    AuthModule,
    CompanyOffersModule,
    ConversationsModule,
    SoftskillsModule,
    SectorsModule,
    CompetenciesModule,
    CacheModule.register(),
    ConfigModule.forRoot({
      envFilePath: ['.env.local', '.env'], 
      isGlobal: true
    })
  ],
  controllers: [AppController],
  providers: [AppService, ConversationsService],
})
export class AppModule {}
