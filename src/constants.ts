/* eslint-disable prettier/prettier */
export const MONO_DB_CONNECTION_STRING =
	process.env.MONO_DB_CONNECTION_STRING || 'mongodb://localhost:27017/UMU_dbApp';

export const DEF_PORT = 3000;

export const SIGNUP_MAIL_ENABLED =
	process.env.SIGNUP_MAIL_ENABLED || true;

export const CLIENT_URL =
	process.env.CLIENT_URL || 'https://upmeup.io';
