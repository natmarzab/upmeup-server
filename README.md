<p align="center">
  <a href="http://upmeup.es/" target="blank"><img src="https://upmeup.es/wp-content/uploads/2022/07/logo_up_me_up_si.png" width="320" alt="upmeup logo" /></a>
</p>

## Description

[Upmeup](https://gitlab.com/librecoop/up-me-up/upmeup-server) backend repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Deployment

Official container images can be found in this repository. The following environment variables must be configured:

  - DB_HOST
  - DB_NAME
  - DB_PORT
  - S3_ENDPOINT
  - S3_PORT
  - S3_SSL
  - S3_ACCESS_KEY
  - S3_SECRET_KEY
  - S3_BUCKET
  - JWT_SECRET
  - MAIL_SMTP_SERVER
  - MAIL_SMTP_PORT
  - MAIL_SMTP_USER
  - MAIL_SMTP_PASSWORD

Here is a sample docker compose:

```yml
# Use root/example as user/password credentials
version: '3.1'

services:
  backend:
    image: registry.gitlab.com/librecoop/up-me-up/upmeup-server:main
    restart: unless-stopped
    ports:
      - 3000:3000
    environment:
      - DB_HOST=mongo
      - DB_NAME=upmeup
      - DB_PORT=27017
      - S3_ENDPOINT=
      - S3_PORT=
      - S3_SSL=
      - S3_ACCESS_KEY=
      - S3_SECRET_KEY=
      - S3_BUCKET=
      - JWT_SECRET=
      - MAIL_SMTP_SERVER=
      - MAIL_SMTP_PORT=
      - MAIL_SMTP_USER=
      - MAIL_SMTP_PASSWORD=
    depends_on:
      - mongo
  mongo:
    image: mongo
    restart: unless-stopped
    volumes:
      - db-files:/data/db

volumes:
  db-files:

```

## License

UpmeUp is [AGPLv3 licensed](LICENSE).

## Misc

### Enable/diable mails

A property is available to enable/disable sending of the signup mail (new user is created)

- Can be defined 
    - in `.env` file (local)
    - or set in the docker-compose YAML
- Defaults to `true` (in `src/constants.ts`)

